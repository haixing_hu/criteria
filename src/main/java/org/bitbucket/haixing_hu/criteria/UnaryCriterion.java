/******************************************************************************
 *
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package org.bitbucket.haixing_hu.criteria;

import javax.annotation.concurrent.Immutable;

import org.bitbucket.haixing_hu.lang.Equality;
import org.bitbucket.haixing_hu.lang.Hash;
import org.bitbucket.haixing_hu.text.tostring.ToStringBuilder;

import static org.bitbucket.haixing_hu.lang.Argument.requireNonNull;

/**
 * A {@link UnaryCriterion} represents a criterion consists of a unary
 * relation of a property.
 *
 * @author Haixing Hu
 */
@Immutable
public final class UnaryCriterion extends Criterion {

  private final String property;
  private final UnaryOperator operator;

  /**
   * Constructs a {@link UnaryCriterion}.
   *
   * @param property
   *          the name of property involved in the new criterion.
   * @param operator
   *          the unary operator.
   */
  public UnaryCriterion(final String property, final UnaryOperator operator) {
    super(CriterionType.UNARY);
    this.property = requireNonNull("property", property);
    this.operator = requireNonNull("operator", operator);
  }

  /**
   * Gets the name of the property involved in this criterion.
   *
   * @return the name of the property involved in this criterion.
   */
  public String getProperty() {
    return property;
  }

  /**
   * Gets the unary operator involved in this criterion.
   *
   * @return the unary operator involved in this criterion.
   */
  public UnaryOperator getOperator() {
    return operator;
  }

  @Override
  public int hashCode() {
    final int multiplier = 7;
    int code = 3;
    code = Hash.combine(code, multiplier, property);
    code = Hash.combine(code, multiplier, operator);
    return code;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    final UnaryCriterion rhs = (UnaryCriterion) obj;
    return Equality.equals(property, rhs.property)
        && Equality.equals(operator, rhs.operator);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
              .append("property", property)
              .append("operator", operator)
              .toString();
  }

  @Override
  public UnaryCriterion clone() {
    return new UnaryCriterion(property, operator);
  }
}
