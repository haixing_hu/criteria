# criteria

A collection of classes used to representing filtering/searching criteria.

## Dependency

The project depends on the following projects of mine:

* [pom-root](https://bitbucket.org/haixing_hu/pom-root): which provides configuration of dependent packages for all projects.
* [commons](https://bitbucket.org/haixing_hu/commons): which provides commonly used classes and functions for my personal Java programming.

## Build

1. Install and configure the JDK 8.0 or above.
2. Checks out the codes of this project and all its depended projects;
3. Build the depended projects in the order of above (**building order is important!**).
4. Build this project.
5. All the projects are managed by [maven](http://maven.apache.org/), so the building is as easy as typing `mvn clean install`. 
